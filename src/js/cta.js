(() => {
  const button = document.getElementById('cta-button');
  const input = document.getElementById('cta-input');

  button.addEventListener('click', () => {
    setTimeout(() => input.focus(), 1000);
  });
})();
