(() => {
  const defaultOptions = {
    classes: {
      arrows: 'carousel__arrows',
      arrow: 'carousel__arrow',
      prev: 'carousel__arrow--prev',
      next: 'carousel__arrow--next',
      pagination: 'carousel__pagination',
      page: 'carousel__pagination-item',
    },
    type: 'loop',
  };

  new Splide(document.getElementById('results-carousel'), {
    ...defaultOptions,
    perPage: 2,
    focus: 'center',
    autoplay: true,
    breakpoints: {
      768: {
        perPage: 1,
      },
    },
  }).mount();
})();
