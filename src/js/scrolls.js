(() => {
  const moveTo = new MoveTo({ duration: 1000 });
  const triggers = document.querySelectorAll('.move-to');

  triggers.forEach((trigger) => {
    moveTo.registerTrigger(trigger);
  });
})();
