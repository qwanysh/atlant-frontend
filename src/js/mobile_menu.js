(() => {
  const menuButton = document.querySelector('.menu-button');
  const menu = document.querySelector('.mobile-menu');
  const menuItems = document.querySelectorAll('.mobile-menu__item');

  const toggleButton = () => {
    menuButton.classList.toggle('menu-button--close');
  };

  const toggleMenu = () => {
    menu.classList.toggle('mobile-menu--opened');
  };

  const toggleBodyScroll = () => {
    const overflow = document.body.style.overflow;
    if (overflow) {
      document.body.style.removeProperty('overflow');
    } else {
      document.body.style.overflow = 'hidden';
    }
  };

  menuButton.addEventListener('click', () => {
    toggleButton();
    toggleMenu();
    toggleBodyScroll();
  });

  menuItems.forEach((menuItem) => {
    menuItem.addEventListener('click', () => {
      toggleButton();
      toggleMenu();
      toggleBodyScroll();
    });
  });
})();
