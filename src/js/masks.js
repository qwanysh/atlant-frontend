(() => {
  const inputs = document.querySelectorAll('.phone-mask');
  const options = {
    mask: '+{7} (000) 000-00-00',
  };

  inputs.forEach((input) => IMask(input, options));
})();
