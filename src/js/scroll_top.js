(() => {
  let lastScrollTop = window.pageYOffset;
  const button = document.querySelector('.mobile-buttons__button--scroll-top');

  window.addEventListener('scroll', () => {
    const scrollTop = window.pageYOffset;

    if (scrollTop === 0 || lastScrollTop - scrollTop < 0) {
      button.classList.add('mobile-buttons__button--hidden');
    } else if (lastScrollTop - scrollTop > 0) {
      button.classList.remove('mobile-buttons__button--hidden');
    }

    lastScrollTop = scrollTop;
  });
})();
